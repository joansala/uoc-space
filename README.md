Práctica final - Space adventure
================================

¿Qué es?
--------



Vídeo de demostración
---------------------

![Demo](Resources/demo.webm)

¿Qué se ha implementado?
------------------------

*

Detalles de la implementación
-----------------------------



La última versión
-----------------

Puede encontrar información sobre la última versión de este software y su
desarrollo actual en https://gitlab.com/joansala/uoc-space

Referencias
-----------

Todas las imágenes y sonidos del juego se han publicado con licéncias que
permiten la reutilización y distribución. Son propiedad intelectual de sus
respectivos authores. Algunos de los recursos se han creado o modificado
exclusivamente para su uso en el juego por el autor, Joan Sala Soler.

[1]  ALEX36917. 2020. Woman_small ow 1 [Efecto de sonido].
     Disponible en: https://freesound.org/people/alex36917/sounds/509722/

[2]  DUSTYROOM. 2014. Dustyroom Casual Game Sounds [Efectos de sonido].
     Disponible en: https://dustyroom.com/casual-game-sfx-freebie

[3]  FOOLBOYMEDIA. 2015. C64 Melody [Búcle musical]. Disponible en:
     https://freesound.org/people/FoolBoyMedia/sounds/275673/

[4]  HARAMIR, Anderson. 2017. Death [Efecto de sonido]. Disponible en:
     https://freesound.org/people/Haramir/sounds/404014/

[5]  KENNEY. 2019. Background Elements Redux [Imágenes]. Disponible en:
     https://www.kenney.nl/assets/background-elements-redux

[6]  KENNEY. 2016. Physics Assets [Imágenes]. Disponible en:
     https://www.kenney.nl/assets/physics-assets

[7]  KENNEY. 2013. Platformer Pack Redux [Imágenes]. Disponible en:
     https://www.kenney.nl/assets/platformer-pack-redux

[8]  LEGNALEGNA55. 2020. Female voice YA WHOO [Efecto de sonido].
     Disponible en: https://freesound.org/people/Legnalegna55/sounds/539216/

[9]  MCINERNEY, Matt. 2009. Orbitron [Tipografía]. Disponible en:
     https://www.theleagueofmoveabletype.com/orbitron

[10] NATHANAELSAMS. 2011. Running on grass with wet feet [Efecto de sonido].
     Disponible en: https://freesound.org/people/nathanaelsams/sounds/127955/

[11] NICK121087. 2014. Glass break [Efecto de sonido].
     Disponible en: https://freesound.org/people/nick121087/sounds/232176/

[12] NOX_SOUND. 2019. Ambiance River Flow Small Loop [Efecto de sonido].
     Disponible en: https://freesound.org/people/Nox_Sound/sounds/479321/
